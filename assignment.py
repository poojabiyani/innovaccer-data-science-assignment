
# coding: utf-8

# In[86]:


import pandas as pd


# In[87]:


import numpy as np


# In[88]:


from sklearn import metrics


# In[89]:


from sklearn.datasets.samples_generator import make_blobs


# In[90]:


import csv


# In[91]:


from sklearn.cluster import AffinityPropagation


# In[92]:


data= pd.read_csv("SampleDataset.csv")


# In[93]:


data['fullname']= data['dob']+' '+data['fn'].astype(str)+' '+data['ln']


# In[94]:


print (data)


# In[95]:


data_male= data.loc[data['gn'] == 'M']


# In[96]:


data_female= data.loc[data['gn'] == 'F']


# In[97]:


male = data_male[['dob', 'fullname']]


# In[98]:


female = data_female[['dob', 'fullname']]


# In[99]:


print (male)


# In[100]:


print (female)


# In[101]:


def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]


# In[102]:


levenshteinDistance("Vladimir Antonio Frometa Garo", "Vladimir Frometa")


# In[103]:


lev_similarity = -1*np.array([[levenshteinDistance(w1,w2) for w1 in male['fullname']] for w2 in male['fullname']])


# In[104]:


words1 = np.asarray(male['fullname'])


# In[105]:


words2 = np.asarray(female['fullname'])


# In[106]:


print (words1)


# In[107]:


lev_similarity = -1*np.array([[levenshteinDistance(w1,w2) for w1 in words1] for w2 in words1])

affprop = AffinityPropagation(affinity="precomputed", damping=0.5)
af1 = affprop.fit(lev_similarity)

exemplars_m = []
strings_m = []
for cluster_id in np.unique(affprop.labels_):
    exemplar = words1[affprop.cluster_centers_indices_[cluster_id]]
    cluster = np.unique(words1[np.nonzero(affprop.labels_==cluster_id)])
    cluster_str = ",".join(cluster)
    exemplars_m.append(exemplar)
    strings_m.append(cluster_str)
    print(" - *%s:* %s" % (exemplar, cluster_str))


# In[108]:


centers = [[1, 1], [-1, -1], [1, -1]]
X, labels_true = make_blobs(n_samples=len(words1), centers=centers, cluster_std=0.5,
                            random_state=0)

cluster_centers_indices = af1.cluster_centers_indices_
labels = af1.labels_

n_clusters_ = len(cluster_centers_indices)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f"
      % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))


# In[109]:


new_exemplars_m= []
late_exemplars_m= []
new_strings_m= []

for (x, y) in zip(exemplars_m, strings_m):
    new_exemplars_m.append(x)
    fn= x.split(' ')[1]
    dob= x.split(' ')[0]
    strs= y.split(',', len(y))
    
    strs_dob= []
    strs_fn= []
    for i in range(0, len(strs)):
        strs_dob.append(strs[i].split(' ')[0])
        strs_fn.append(strs[i].split(' ')[1])
     
    new_str= []
    for j in range(0, len(strs_dob)):
        if((dob == ''.join(strs_dob[j])) and (fn == ''.join(strs_fn[j]))):
            new_str.append(strs[j])
        else:
            late_exemplars_m.append(strs[j])
                  
    new_strings_m.append(new_str)

total_exemplars_m= new_exemplars_m + late_exemplars_m


# In[110]:


for (x, y) in zip(new_exemplars_m, new_strings_m):
    print(" - *%s:* %s" % (x.split(' ')[0]+','+x.split(' ', 1)[-1], y))

for x in late_exemplars_m:
    print(" - *%s:*" % (x))


# In[111]:


lev_similarity = -1*np.array([[levenshteinDistance(w1,w2) for w1 in words2] for w2 in words2])

affprop = AffinityPropagation(affinity="precomputed", damping=0.5)
af2 = affprop.fit(lev_similarity)

exemplars_f = []
strings_f = []
for cluster_id in np.unique(affprop.labels_):
    exemplar = words2[affprop.cluster_centers_indices_[cluster_id]]
    cluster = np.unique(words2[np.nonzero(affprop.labels_==cluster_id)])
    cluster_str = ",".join(cluster)
    exemplars_f.append(exemplar)
    strings_f.append(cluster_str)
    print(" - *%s:* %s" % (exemplar, cluster_str))


# In[112]:


centers = [[1, 1], [-1, -1], [1, -1]]
X, labels_true = make_blobs(n_samples=len(words2), centers=centers, cluster_std=0.5,
                            random_state=0)

cluster_centers_indices = af2.cluster_centers_indices_
labels = af2.labels_

n_clusters_ = len(cluster_centers_indices)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f"
      % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))


# In[113]:


new_exemplars_f= []
late_exemplars_f= []
new_strings_f= []

for (x, y) in zip(exemplars_f, strings_f):
    new_exemplars_f.append(x)
    fn= x.split(' ')[1]
    dob= x.split(' ')[0]
    strs= y.split(',', len(y))
    
    strs_dob= []
    strs_fn= []
    for i in range(0, len(strs)):
        strs_dob.append(strs[i].split(' ')[0])
        strs_fn.append(strs[i].split(' ')[1])
     
    new_str= []
    for j in range(0, len(strs_dob)):
        if((dob == ''.join(strs_dob[j])) and (fn == ''.join(strs_fn[j]))):
            new_str.append(strs[j])
        else:
            late_exemplars_f.append(strs[j])
                  
    new_strings_f.append(new_str)

total_exemplars_f= new_exemplars_f + late_exemplars_f


# In[114]:


for (x, y) in zip(new_exemplars_f, new_strings_f):
    print(" - *%s:* %s" % (x.split(' ')[0]+','+x.split(' ', 1)[-1], y))

for x in late_exemplars_f:
    print(" - *%s:*" % (x))


# In[115]:


fullist = []
variationlist = []
for lis in new_strings_m:
    temp = []
    for item in lis:
        item = item[9:]
        temp.append(item)
    variationlist.append(temp)
    
for (x, y) in zip(new_exemplars_m, variationlist):
    data = x.split(" ", 1)[1] + ',' + x.split(" ")[0] + ',' + 'M' + ',' + ','.join(y)
    lis = data.split(",")
    print (lis)
    fullist.append(lis)
    
for x in late_exemplars_m:
    data = x.split(" ", 1)[1] + ',' + x.split(" ")[0] + ',' + 'M' + ',' + '-'
    lis = data.split(",")
    print (lis)
    fullist.append(lis)

df = pd.DataFrame(fullist)
df.to_csv('output.csv', index=False, header= ["Name", "DoB", "Gender", "Variation", "Variation", "Variation", "Variation", "Variation"])


# In[116]:


fullist = []
variationlist = []
for lis in new_strings_f:
    temp = []
    for item in lis:
        item = item[9:]
        temp.append(item)
    variationlist.append(temp)
    
for (x, y) in zip(new_exemplars_f, variationlist):
    data = x.split(" ", 1)[1] + ',' + x.split(" ")[0] + ',' + 'F' + ',' + ','.join(y)
    lis = data.split(",")
    print (lis)
    fullist.append(lis)
    
for x in late_exemplars_f:
    data = x.split(" ", 1)[1] + ',' + x.split(" ")[0] + ',' + 'F' + ',' + '-'
    lis = data.split(",")
    print (lis)
    fullist.append(lis)

df = pd.DataFrame(fullist)
with open('output.csv', 'a') as f:
    df.to_csv(f, index=False, header=False)
