# Innovaccer-Analytics-Assignment

The project aims to solve the problem of deduplication. The name of a person can have many variations. The problem is to cluster all the variations of the name of a unique person together. For example: Following variations are same as Vladimir Frometa: 

* Vladimir Antonio Frometa Garo 
* Vladimir A Frometa Garo 
* Vladimir Frometa 
* Vladimir Frometa G 
* Vladimir A Frometa 
* Vladimir A Frometa G 

## Dataset

The sample dataset ('SampleDataset.csv') is a csv file that contains 103 records and 4 attributes. The attributes are:

* fn (First Name) 
* ln (Last Name) 
* dob (Date of Birth) 
* gn (Gender) 

So, to identify the variations of name of a unique person both dob and gn is to be considered. Therefore, 

* Persons with same name/ variation but different gender are present in different clusters. 
* Persons with same name/ variation but different dob are present in different clusters. 
* Only persons with same name/ variation and same dob and gender are present in the same cluster. 

## Input

The input has to be similar to the format of the sample dataset. It should contain all 4 attributes and any number of records.

![Screenshot](/Screenshots/input.png)

## Environment to execute code

The code is written in Python. It is attached in both .py and .ipynb format. 

The .py file ('assignment.py') can be executed on terminal with Python and necessary packages installed on the system. Else directly run the code on browser at: https://try.jupyter.org/ by uplaoding the .ipynb file ('assignment.ipynb') and the input dataset with the file name 'SampleDataset.csv'. 

The necessary packages that are needed to be installed on the system to run the code through terminal are:

* Pandas
* Numpy
* Sklearn

## Output

The output produced will be present in the file 'output.csv'. It has the following columns: 

* Name (Full Name) 
* DoB (Date of Birth) 
* Gender 
* Variation 

Each record contains details of a unique person. 

The Name column contains the full name of the unique person. The DoB and Gender are the date of birth and gender of the unique person respectively. The Variation column contains the variations of the name of the unique person. 

So, the total rows represents the number of unique persons in the input dataset. And the Variation columns provide the variations of the name of that person in the input dataset.

For example: the first record from the following output file screenshot has:

* Name : ROY MICHAELSON JR
* DoB : 25/10/53
* Gender : M
* Variation : ROY MICHAELSON
* Variation : ROY MICHAELSON JR

This means that ROY MICHAELSON JR is a unique person with date of birth: 25/10/53 and gender: male. This person has records with variation of name as ROY MICHAELSON and ROY MICHAELSON JR in the input dataset.

![Screenshot](/Screenshots/output.png)
